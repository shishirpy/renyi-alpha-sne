# README #

In this repository we tryout the t-SNE algorithm using Renyi divergence as our cost function and not the usual KL divergence used in the t-SNE paper. Since, alpha is a parameter for the Renyi divergence, in this repository we tryout various values of alpha.
